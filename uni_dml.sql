insert into uni_university(id,name) values(1,'AzTU');
 commit;
 
 insert into uni_faculties(id,phone,uni_university_id,name) values(1,0557854717,1,'ITF');
 commit;
 
 insert into uni_groups(id,uni_faculty_id,name) values(1,1,'19.1');
 commit;
 
 insert into uni_subjects(id,name) values(1,'Informasiya tehlukesizliyi');
 commit;
 
 
 insert into uni_teachers(id,first_name,last_name,birthdate,phone,mail,uni_subjects_id)  
 values(1,'Leyla','Babayeva',to_date('02.12.1998','DD.MM.YYYY'), 0558791246,'uni_teacher@gmail.com',1);
 commit;


insert into uni_education_form(id,name) values(1,'eyani');
 commit;

insert into uni_sector(id,name) values(1,'az');
 commit;

 
 insert into uni_groups_teachers (uni_groups_id,uni_teachers_id) values(1,1);
 commit;

 insert into uni_student(id,first_name,last_name,birthdate,phone,mail,gpa,uni_education_form_id,uni_sector_id,uni_groups_id)  
 values(1,'Asmar','Shukurlu',to_date('03.07.2002','DD.MM.YYYY'),0555555555,'asmer@gmail.com',98,1,1,1);
 commit;

